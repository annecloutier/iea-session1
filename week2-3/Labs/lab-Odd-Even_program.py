# Lab: Odd-Even Program (our first program that does something)

#     prompt the user to enter a number
#     read input from the user
#     convert the input to an integer
#     tell the user whether the number entered was odd or even

number = int(input('Enter a number: '))
if number % 2 == 0:
    print(number, 'is EVEN')
else:
    print(number, 'is ODD')