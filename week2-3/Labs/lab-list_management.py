# Lab: List Management/Sorting

#     write a program to read in words
#     if the word begins with a vowel, put it in "vowel" list, 
#       otherwise put it in the "consonant" list
#     when the user types "quit", stop and print out the sorted list of 
        # words that begin with vowels, and the sorted list of words that begin with consonants



word_list = ['survey','examples','homework','labs','expressions','bash','python','options','jupyter','virtual','assignment','aggravation','idiot']
vowels = list('aeiou')
vowel_list = []
consonant_list = []

for word in word_list:
    if word[0] in vowels:
        vowel_list.append(word)
    else:
        consonant_list.append(word)
print(sorted(vowel_list))
print(sorted(consonant_list))

# words_not_starting_with_vowel = [word for word in words if word[0] != vowels]
# print(words_starting_with_vowel)


# for words in file:
#     if words[0] == vowels:
#         print
#     #vowel_list.remove(words)
# #print(vowel_list)
# print(words)

# words_starting_with_vowel = [word for word in words if word[0] in vowels]
# print(words_starting_with_vowel)

#----------------------EXAMPLE-----------------------------------
# #words = []
# file = open('wordlist.txt')
# # type(file)
# words = [list(file)]
# #for line in file:
# #    words.append(line.strip())
# print(words)
# file.close()

# # with open('wordlist.txt') as file:
# #     words = list(file)
#----------------------EXAMPLE-----------------------------------