# Lab: Input

#   write Python code to prompt the user for a year and print out the year the user entered
#   your output will look something like this:

        # Enter a year: 2017
        # You entered 2017

year_entered = input('Please enter a year: ')
print('The year you entered was:', year_entered)