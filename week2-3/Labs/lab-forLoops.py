# FOR LOOP LAB
# write a Python program which asks the user for a string and 
# then outputs the same string with each character duplicated
#    e.g., if the user enters salesforce, your program will output ssaalleessffoorrccee

#dupChar = list(input('Enter your name: '))

# def split(dupChar):
#     return [char for char in dupChar]

dupChar = input('Enter your name: ')
newEmpty = ''
for char in dupChar:
    #print(char * 2, end='')
    newEmpty += char * 2 # adds to the string using the variable
print(newEmpty)



# end='' Python’s print() function comes with a parameter called ‘end’. 
# By default, the value of this parameter is ‘\n’, i.e. the new line character. You can end a print statement with any character/string using this parameter.