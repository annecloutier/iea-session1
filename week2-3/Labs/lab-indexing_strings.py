# Lab: Indexing Strings

#     prompt the user and read a string
#     prompt the user for an index
#     use the index to print out the character at that offset (e.g., if user enters '3', you would print out the [3] character of the string
#     what happens if you hit return (i.e., enter an "empty string")?
#     what happens if you set the zeroth character of the string you read to 'x'
#         name[0] = 'x'

prompt = input("Enter your dog's name: ")
index = int(input('Enter a number: '))
print(prompt[index])