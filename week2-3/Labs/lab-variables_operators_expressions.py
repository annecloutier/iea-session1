# Lab: Variables, Operators, and Expressions

#     create a variable named minutes and give it an initial value of 28435
#     create a variable named hours and set it equal to minutes divided by 60
#     create a variable named days and set it equal to hours divided by 24
#     try both / and // and be sure you understand how they differ
#         / will provide a float
#         // will provide an integer
#     consider the following expressions and then enter them into Jupyter to verify your understanding
#         days * 24 + hours * 60
#         days * (24 + hours) * 60
#         (days * 24) + (hours * 60)

minutes = 28435
hours = minutes/60
hours2 = minutes//60
days = hours/24
days2 = hours//24
print('Minutes: ', minutes)
print('Hours_float: ', hours)
print('Hours_int: ', hours2)
print('Days_float: ', days)
print('Days_int: ', days2)