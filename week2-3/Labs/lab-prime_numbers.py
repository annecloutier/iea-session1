# LAB: Finding Prime Numbers

    # write a program to print out the prime numbers between 10 and 30
    # a number is prime if it's only divisible by 1 and itself
    # A prime number is a numeral that is greater than 1 and cannot be divided evenly by any other number except 1 and itself.
    # If a number can be divided evenly by any other number not counting itself and 1, it is not prime
    # algorithm:
    #     for each number 10 to 30
    #         try to divide in all of the numbers up to (but not including) the current number
    #         if any lower number divides in evenly, the number is not prime
    #         if NONE of the lower numbers divide in evenly, the number IS prime
    # later, if there's time, we'll look at another way to find prime numbers that was discovered by Eratosthenes

# https://www.tutorialgateway.org/python-program-to-print-prime-numbers-from-1-to-100/
# First, we used For Loop to iterate a loop between 1 and 100 values. Within the for loop,

#     - We used another For loop to check whether the number is divisible or not. 
#     If true, count incremented, and break statement skip that number.
#     - Next, if statement checks whether the count is zero, and the given number is not equal to 1. 
#     If it is true, it prints the number because it is a Prime Number.

for num in range(10,30):
    count = 0
    for integer in range(2, (num//2 + 1)):
        if (num % integer == 0):
            count += 1
            break
    if (count == 0 and num != 1):
    else:
        print(num)

# notice the range increases by 1 and removes the argument of count
for num in range(10,31):
    for integer in range(2, (num//2 + 1)):
        if (num % integer == 0):
            count += 1
            break
    if (count == 0 and num != 1):
        print(" %d" %Number, end = '  ')

#-----------------------------------------------------------------------------------------------------
# Python Program to find Prime Number
# https://www.tutorialgateway.org/python-program-to-find-prime-number/
 
# Number = int(input(" Please Enter any Number: "))
# count = 0

# for integer in range(2, (Number//2 + 1)):
#     if(Number % integer == 0):
#         count = count + 1
#         break

# if (count == 0 and Number != 1):
#     print(" %d is a Prime Number" %Number)
# else:
#     print(" %d is not a Prime Number" %Number)

#-----------------------------------------------------------------------------------------------------