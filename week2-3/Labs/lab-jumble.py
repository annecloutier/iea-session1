# LAB: JUMBLE (Word Scrambling)
# write a program which plays the jumble word game, 
# i.e., it will present you with a scrambled word and you have to come up with the correctly spelled word

#     you can use the random module for this
#     random.choice(container) will return a random item from the container
#     random.shuffle(container) will shuffle a container so the items are scrambled
#     you can't shuffle a string, so you'll need to put the characters into a list using the list() function, then shuffle the list, then put the back into a string using join()
import random

farm_animals = ['cows', 'chickens', 'horses', 'pigs', 'goats']
pets = ['dogs', 'cats', 'chickens', 'gerbils']
farm_animals2  = ['alpacas', 'emus', 'sheep', 'ostriches', 'kangaroos']
all_animals = ['cows', 'chickens', 'horses', 'pigs', 'goats', 'dogs', 'cats', 'chickens', 'gerbils', 'alpacas', 'emus', 'sheep', 'ostriches', 'kangaroos']


jumbled_word = random.choice(all_animals) #choose a random word
jumble_letters = (list(all_animals))
print(''.join(all_animals))
print(random.choice(all_animals))
print(random.shuffle(all_animals))




for letters in jumble_letters:


# The "empty list" is just an empty pair of brackets [ ]. The '+' works to append two lists, so [1, 2] + [3, 4] yields [1, 2, 3, 4] (this is just like + with strings).