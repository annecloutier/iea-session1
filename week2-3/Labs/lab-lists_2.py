# Lab: Lists_2
#######################################################################

# create an empty list

#write Python code to repeatedly ask for the user for a word until the work is 'quit'

#add each word to the list

#after the user types 'quit' print every other word (first, third, fifth, etc.)

#then print every other word (second, fourth, sixth, etc.)

word_list = []
while word_list != 'quit':
    word_list.append(input('Please provide a new word to add to the list: '))
    if word_list[-1] == 'quit': # the [-1] will check if the last word entered was the word quit
        break
print(word_list[:-1]) # prints all but the last word which was quit which made the loop  break


# list_names = ['JR', 'Spencer', 'Alex']
# other_instructors = ['Dave', 'Rick']
# list_names.insert(3, other_instructors)
# #print(list_names)
# #print(list_names[3][1])
# #list_names.extend('Kameron')
# print(list_names)