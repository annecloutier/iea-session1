# Lab: Variables

#     create a variable named quantity and give it an integer value
#     verify that quantity has the value you gave it
#     verify that quantity is an integer
#     create a variable named company and give it a value of 'mycompany'
#     verify that company is a string and that its value is 'mycompany'

quantity_var = 17
print(quantity_var)
print(type(quantity_var))
company_var = 'mycompany'
print(company_var, type(company_var))