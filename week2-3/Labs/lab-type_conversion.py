# Lab: Type Conversion

#     write Python code to prompt the user to enter a year
#     ...then reads input from the user
#     ...then converts what was read into an integer
#     print out the final result and verify that it's an integer
#     your output should look something like this:


# Enter a year: 2017
# The year you entered was 2017.

# <class 'int'>

year = int(input('Enter a year: '))

print('The year you entered was: ', year, type(year))