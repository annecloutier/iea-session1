# Lab: Strings

#     read in two separate strings from the user
#     create a new string which consists of the second string followed by a space, followed by the first string
#     e.g, "hello" and "there" would become "there hello"

user_string_a = input('Say anything: ')
user_string_b = input('Say something else: ')
my_string = user_string_b + ' ' + user_string_a
print(my_string)