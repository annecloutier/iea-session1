string = ""
while len(string) != 5:
    string = input("Please enter a 5-letter string: ")
    if len(string) != 5:
        print('Your code must be exactly 5-letters, please re-enter')
    else:
        print("Thank you!")

# string = input("Please enter a 5-letter string: ")
# while len(string) != 5:
#     print('YOUR ENTRY MUST BE 5 LETTERS LONG...')
#     string = input("Please enter a 5-letter string: ")
# print("Thank you!")
