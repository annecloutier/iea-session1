# Lab: Leap Year Program

#     prompt the user to enter a year
#     read input from the user
#     convert the input to an integer
#     tell the user whether the year entered is a leap year or not
#         a year is a leap year if
#         it's divisible by 4 AND
#         it's not divisible by 100 (i.e., 1900 was not a leap year) UNLESS
#         it's also divisible by 400 (i.e., 2000 was a leap year)

year = int(input('Enter a year: '))
if (year % 4 == 0) and (year % 100 != 0 or year % 400 == 0):
    print(year, 'was a leap year')
else:
    print(year, 'was not a leap year')