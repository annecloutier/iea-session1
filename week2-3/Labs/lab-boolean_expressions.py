# Lab: Boolean Expressions

#     write a Boolean expression to determine whether the variable xyz is greater than 100 (you will have to define the variable first)
#     write a Boolean expression to determine whether the variable company is equal to the string 'salesforce'

xyz = 11
company = 'salesforce'
print(xyz > 100)
print(xyz == company)