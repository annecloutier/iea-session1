# banner = '-'
# print(banner*50)

# year = int(input('Enter a year: '))
# # convert from string to integer

# if year > 2000:
#     print('blah', 'indented statement', 'something',
#             sep='\n')
#     print('another')

# print('after the if statement')

# number = int(input('enter a number: '))
# print(type(number))
# number

# if number % 2 == 0:
#     print('your number is even')
# else:
#     print('your number is strange')

# leap_year = int(input('enter a year: '))
# leap_year

# if (leap_year % 4 == 0) and (leap_year % 100 != 0 or leap_year % 400 == 0):
#     print('the year you entered is a leap year!')
# else:
#     print('the year you entered is just another year...')



# leap_year = int(input('enter a year: '))
# leap_year

# if (leap_year % 4 == 0) and (leap_year % 100 != 0 or leap_year % 400 == 0):
#     print(f'{leap_year} is a leap year!')
# else:
#     print(f'{leap_year} is just another year...')

# storyWhen = input('when was this? ')
# storyWho = input('who is the story about? ')
# storyWhat = input('what happened? ')
# storyEnd = 'life isn\'t fair so pick yourself up and get over it! The End.'

# storyTelling = print('Once upon a time...', storyWhen + ', ' + storyWho + ' ' + storyWhat + ' she realizes that ' + storyEnd)
# storyTelling


# Online Example:
# number = input("Enter a whole number ")
# answer = int(number) * 5 #converts the variable number to an integer and multiplies it by 5.
# print(answer)

# string = 'Frank Benedict eats jam in the morning'
# #print(string[6:9] + string[20:23] + string[24:27] + string[:5] + 'lin')
# code = string[6:9] + string[20:23] + string[24:27] + string[:5] + 'lin'
# print(code)

# words = ['apple', 'orange', 'pear', 'milk', 'otter', 'snake', 'iguana',
#          'tiger', 'eagle']
# vowel=[]
# for vowel in words:
#     if vowel[0]=='aeiou':
#         # words.append(vowel)
#         print (vowel)

# words = ['apple', 'orange', 'pear', 'milk', 'otter', 'snake','iguana','tiger','eagle']
# for word in words:
#     if word[0] in 'aeiou':
#         print(word)

# words_starting_with_vowel = [word for word in words if word[0] in 'aeiou']
# print(words_starting_with_vowel)    

# import time
# #time.ctime()
# #print(time.ctime())
# def show_default_time(default_time = time.ctime()):
#     print(default_time)
#     show_default_time()

### Mutable Default Vaules ###
# def add_spam(menu = []):
#     menu.append("spam")
#     return menu
#     breakfast = ['bacon', 'eggs', 'potatoes']
#     add_spam(breakfast)
# print(add_spam)
