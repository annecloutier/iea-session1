# Quora Challenge 1:
# The quora challenge framework
# In order to easily take part in these challenges, the first thing to do is to install the quorachallenge framework.
# At your command prompt :
#     $ pip install quorachallenge>=1.0.3
# and make sure you read the documentation : Quora Challenge: Quora Challenge Framework. 
# The framework provides easy access to the challenge description and to automated testing of your solution.

# The Challenge:
# Once you have installed the framework, you need to complete the challenge :
# The challenge name is ‘segment_counter’ (you will need this for the framework).
# You need to write a single function which takes a string as a positional argument, and counts the total number of ‘segments’ within the string. 
# A segment is simply defined as a uninterrupted sequence of the same characters.
# Example :
#     Input string : abbccbbba
#     Segments : a, bb, cc, bbb, a
#     Segment Count : 5

# Example:
#     Input string : abca
#     Segments : a, b, c, a
#     Segment Count : 4

# This challenge is just the the start of a series which will build into a more complex function.
# Using the framework
# Read Getting Started - Using the Framework for details on how to use the framework to automatically test your solution. 
# Remember - the challenge name in this case is ‘segment_counter’.
# For example if your function is called my_segment_counter then this code will automatically test your function (the challenge has 1000 defined test cases).
#     import quorachallenge
     
#     @quorachallenge.autotest('segment_count')
#     def my_segment_counter(the_string):
#     	# Your code here
#     	# More code
#     	......
# Line 3 is the critical item which triggers the automated testing.
# And Finally
# Good luck, and keep your eyes open for the next challenge - which will build on this one.
# ---------------------------------------------------------------------------------------------------------------------------------------
# the_string = input('Enter an alphanumeric string: ')
# segments = list(the_string)

# def my_segment_counter(the_string):
#     for letters in segments:
#         segment_count = letters.count(segments)
#         letters.count(segments)
#     print(segments)
#     print(segment_count)
# ---------
# def my_segment_counter():
#     the_string = input('Enter an alphanumeric string: ')
#     segments = list(the_string)
#     for letters in segments:
#         segment_count = letters.count(segments)
#         letters.count(segments)
#     print(segments)
#     print(segment_count)
# ---------
# my_segment_counter()

# import quorachallenge
    
# @quorachallenge.autotest('segment_count')

#the_string = input('Enter an alphanumeric string: ')

# #segments = list(the_string)
# segment_count = []
# #segment_count = segments.count(letters)

# def my_segment_counter('ssucking'):
#     prev_letter = ''
#     for letters in segments:
#         if letters != prev_letter:
#             segment_count.append(letters)
#             count += 1
#         prev_letter = letters
#         segment_count = letters.count(segments)
#         #print(segments)
# print(segment_count)
# #print(list(the_string))


## Basic 
# the_string = input('Enter an alphanumeric string: ')
# segment_count = 0
# for letters in the_string:
#     segment_count += 1
# print(the_string)
# print(segment_count)

# import quorachallenge
    
# @quorachallenge.autotest('segment_count')
# def my_segment_counter():
#     the_string = input('Enter an alphanumeric string: ')
#     segments = []
#     my_segment_count = 0
#     prev_letter = ''
#     multiple_letters = ''
#     for letters in the_string:
#         if letters != prev_letter:
#             segments.append(letters)
#             my_segment_count += 1
#         else:
#             multiple_letters = segments[my_segment_count -1]
#             multiple_letters += letters
#             segments[my_segment_count -1] = multiple_letters
#         prev_letter = letters
#     print(segments)
#     print(my_segment_count)

# my_segment_counter()

def my_segment_counter():
    the_string = input('Enter an alphanumeric string: ')
    segments = []
    my_segment_count = 0
    prev_letter = ''
    multiple_letters = ''
    for letters in the_string:
        if letters != prev_letter:
            segments.append(letters)
            my_segment_count += 1
        else:
            multiple_letters = segments[my_segment_count -1]
            multiple_letters += letters
            segments[my_segment_count -1] = multiple_letters
        prev_letter = letters
    print(segments)
    print(my_segment_count)

my_segment_counter()